#!/usr/bin/python3

import requests

# define base URL
MAGICURL = "https://api.magicthegathering.io/v1/cards?page=2&set=P02"

def main():

    cardresp = requests.get(MAGICURL)
    cardresp = cardresp.json()

    #create a list of cards with toughness = 1
    toughcards = []
    print("\ncreatures with toughness 1 are:\n")
    for card in cardresp["cards"]:
        #print(card.get("name"))
        if card.get("toughness") == "1":
            print(card.get("name"))
            toughcards.append({"name":card.get("name"),"toughness":card.get("toughness"),"power":card.get("power"),"flavor":card.get("flavor")})

    #print("\ncards with toughness 1 are: \n")
    #print(toughcards)
    print(f"\nTotal number of cards with toughness 1 is {len(toughcards)} \n")

if __name__ == "__main__":
    main()

