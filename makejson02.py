#!/usr/bin/python3
"""Reviewing how to parse json | Alta3 Research"""

# JSON is part of the Python Standard Library
import json

def main():
    """runtime code"""
    ## create a blob of data to work with
    hitchhikers = [{"name": "Zaphod Beeblebrox", "species": "Betelgeusian"},
      {"name": "Arthur Dent", "species": "Human"}]

    ## display our Python data (a list containing two dictionaries)
    print(hitchhikers)

    #with open("galaxyguide.json","w") as zfile:
    #    json.dump(hitchhikers, zfile)

    jsonstr=json.dumps(hitchhikers)
    print(jsonstr)

if __name__ == "__main__":
    main()
