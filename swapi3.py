#!/usr/bin/env python3

from pprint import pprint

import requests

URL = "https://swapi.dev/api/planets/1/"

def main():
    resp = requests.get(URL);

    if(resp.status_code == 200):
        planet = resp.json()
        pprint(planet)
    else:
        print("That is not a valid URL")

if __name__ == "__main__":
    main()
