#!/usr/bin/python3

from flask import Flask
from flask import request
from flask import redirect
from flask import url_for
from flask import session
from flask import render_template

app = Flask(__name__)

app.secret_key= "random random RANDOM!"

groups = [{"hostname": "hostA","ip": "192.168.30.22", "fqdn": "hostA.localdomain"},
          {"hostname": "hostB", "ip": "192.168.30.33", "fqdn": "hostB.localdomain"},
          {"hostname": "hostC", "ip": "192.168.30.44", "fqdn": "hostC.localdomain"}]

@app.route("/")
def index():
    return render_template("hosts.conf.j2", hosts = groups)

@app.route("/newhost")
def newhost():
    return render_template("newhost.html")

@app.route("/addhost", methods = ["POST"])
def addhost():
    print(f"hostname = { request.form.get('hostname') }")
    newhost = {}
    newhost["hostname"] = request.form.get("hostname")
    newhost["ip"] = request.form.get("ip")
    newhost["fqdn"] = request.form.get("fqdn")
    groups.append(newhost)

    #return render_template("hosts.conf.j2", hosts = groups)
    return redirect(url_for("index"))

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=2224)
