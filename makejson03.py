#!/usr/bin/python3
"""opening a static file containing JSON data | Alta3 Research"""

# JSON is part of the Python Standard Library
import json

def main():
    """runtime code"""

    with open("datacenter.json","r") as datacenter:
        datacenterstring = datacenter.read()

    print(datacenterstring)
    print(type(datacenterstring))

    datacenterdataset = json.loads(datacenterstring)
    print(datacenterdataset)

if __name__ == "__main__":
    main()
